run docker compose 

```bash
docker-compose up -d --build
 ```

 start bash vault
 
```bash
docker-compose exec vault bash
 ```

 inside bash vault running

```bash
vault operator init

vault operator unseal 
## do usnsel with 3 of 5 keys

## if sealed is false, run
vault login

## enable secret kv path secret/
vault secrets enable -path=secret/ kv

#add path spring-vault (this is application name on bootstrap.yml) under secret, dev will be identify profile active on bootstrap.yml
vault kv put secret/spring-vault/dev db.username=root db.password=root

## add policies for app, we will use this policies for spring app
vault policy write app /vault/policies/app-policy

## use this token on bootstrap.yml vault.token
vault token create -policy=app
 ```
 
