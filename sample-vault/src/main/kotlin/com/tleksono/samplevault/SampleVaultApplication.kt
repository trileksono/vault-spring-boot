package com.tleksono.samplevault

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SampleVaultApplication : CommandLineRunner {

    @Autowired
    private lateinit var properties: VaultProperties

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<SampleVaultApplication>(*args)
        }
    }

    override fun run(vararg args: String?) {
        val log = LoggerFactory.getLogger(this.javaClass.simpleName)

        log.info(properties.username)
        log.info(properties.password)
    }
}
