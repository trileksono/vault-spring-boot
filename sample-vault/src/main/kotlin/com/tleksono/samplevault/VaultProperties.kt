package com.tleksono.samplevault

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("db")
class VaultProperties {
    lateinit var username: String
    lateinit var password: String
}